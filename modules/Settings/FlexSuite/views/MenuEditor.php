<?php


class Settings_FlexSuite_MenuEditor_View extends Settings_Vtiger_Index_View {

    public function process(Vtiger_Request $request) {
        $adb = \PearDatabase::getInstance();
        $viewer = $this->getViewer($request);

		// $elements = array();
		// $viewer->assign('ELEMENTS', $elements);
		
        $viewer->view('MenuEditor.tpl', 'Settings:FlexSuite');
    }

    function getHeaderScripts(Vtiger_Request $request) {
        $headerScriptInstances = parent::getHeaderScripts($request);
        $moduleName = $request->getModule();

        $jsFileNames = array(
//            "~modules/$moduleName/views/resources/js/RedooUtils.js",
        );

        $jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
        $headerScriptInstances = array_merge($headerScriptInstances, $jsScriptInstances);

        return $headerScriptInstances;
    }

    function getHeaderCss(Vtiger_Request $request) {
        $headerScriptInstances = parent::getHeaderCss($request);
        $moduleName = $request->getModule();

        $cssFileNames = array(
//            "~layouts/".Vtiger_Viewer::getLayoutName()."/modules/$moduleName/resources/style.css",
        );

        $cssScriptInstances = $this->checkAndConvertCssStyles($cssFileNames);
        $headerStyleInstances = array_merge($headerScriptInstances, $cssScriptInstances);
        return $headerStyleInstances;
    }	
}